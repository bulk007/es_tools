from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="wfsr",
    version="0.1",
    author="Lennert van Overbeeke",
    author_email="lennert.vanoverbeeke@wur.nl",
    description="""A Python module with scripts and data files supporting work done at
        Wageningen Food Safety Research (WFSR), part of Wageningen University & Research. 
        Functionality includes translation, media localization, and conversion of 
        agricultural products and countries.""",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://git.wur.nl/overb015/wfsr",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Framework :: IPython",
        "Framework :: Jupyter",
        "Intended Audience :: Science/Research",
        "Natural Language :: English",
        "Topic :: Scientific/Engineering :: Information Analysis"
    ],
    keywords="""data analysis python pandas feed food safety wageningen 
        research wur wfsr translation media localization conversion agricultural
        products countries""",
    project_urls={
        'Organization': 'https://www.wur.nl/en/Research-Results/Research-Institutes/FoodSafetyResearch.htm',
        'Source': 'https://git.wur.nl/overb015/wfsr',
        'Tracker': 'https://git.wur.nl/overb015/wfsr/issues',
    },
    python_requires='>=3',
    install_requires=[
        'pandas',
        'elasticsearch',
        'googletrans',
        'import_ipynb',
        'selenium',
        'pyshp==2.1.0',
#        'Shapely==1.6.4.post1',
        'Shapely',
        'xlrd',
        'openpyxl',
        'IPython',
        'nbformat',
        'flask',
        'pycountry',
    ],
    packages=find_packages(),
    include_package_data=True,
)