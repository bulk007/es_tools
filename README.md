# WFSR

A Python module with elasticsearch tools and various others for WFSR projects.

This project was copied from Lennert van Overbeek's private repostitory for the sake of continuity of the code.

## Installation
  `pip install git+https://git.wur.nl/bulk007/es_tools.git@master`  
