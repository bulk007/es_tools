# Import pip for installing new modules
from pip._internal import main

# List of module names to import
dependencies = [
    'pandas'
]

# Iterate over the module names
for dep in dependencies:
    try:
        # Import module if available
        # This notation is required for module names in string format
        __import__(dep)
    except ModuleNotFoundError:
        # Use 'pip install' if the module is not available
        main(['install', dep])
        __import__(dep)